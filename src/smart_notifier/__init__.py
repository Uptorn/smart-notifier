#
#This file is part of smart-notifier, a graphical disk health notifier
#
#Copyright (C) 2005, 2006  Brian Sutherland <jinty@web.de>
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

import dbus
import dbus.service
import dbus.glib # This **HIGHLY** unintuitive piece of code is to
                 # monkey patch some stuff and make other stuff work.
                 # Fun with capital F

# Globals
BUS = dbus.SystemBus()
SERVICE = "/smart_notifier/DbusService"
SERVICE_NAME = SERVICE.replace('/', '.')[1:]

UD_SERVICE = SERVICE + "/WarnUser"
UD_INTERFACE = SERVICE_NAME + ".WarnUser"

def notify(msg):
    bus_name = dbus.service.BusName(SERVICE_NAME, bus=BUS)
    object = WarnUser(bus_name, UD_SERVICE)
    object.warn_user(msg)


class WarnUser(dbus.service.Object):

    @dbus.service.signal(UD_INTERFACE)
    def warn_user(self, msg):
        pass
